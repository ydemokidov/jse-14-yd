package com.t1.yd.tm.service;

import com.t1.yd.tm.api.repository.ICommandRepository;
import com.t1.yd.tm.api.service.ICommandService;
import com.t1.yd.tm.model.Command;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
